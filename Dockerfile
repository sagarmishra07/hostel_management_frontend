FROM node:14.18.1-alpine3.14
WORKDIR /usr/src/app
COPY /dist/spa ./
RUN npm install
EXPOSE 3000
CMD [ "node", "index.js" ]
