import { RouteConfig } from 'vue-router';

const routes = (store) => {
  return [
    {
      path: '/',
      component: () => import('layouts/DashboardLayout.vue'),

      children: [
        {
          path: '',
          name: 'Landing',
          component: () => import('pages/Dashboard/Landing.vue'),
        },
        {
          path: 'students',
          name: 'student',
          component: () => import('src/pages/Dashboard/Student.vue'),
        },
        {
          path: 'teachers',
          name: 'teacher',
          component: () => import('src/pages/Dashboard/Teacher.vue'),
        },
        {
          path: 'studentexpenses',
          name: 'studentexpenses',
          component: () => import('src/pages/Dashboard/StudentExpenses.vue'),
        },
        {
          path: 'studentexpenses/:date',
          name: 'studentexpensesdetails',
          component: () =>
            import('src/components/modules/Students/studentExpenses.vue'),
        },
        {
          path: 'teacherexpenses',
          name: 'teacherexpenses',
          component: () => import('src/pages/Dashboard/TeacherExpenses.vue'),
        },
      ],
    },
  ];
};

// Always leave this as last one,
// but you can also remove it

export default routes;
