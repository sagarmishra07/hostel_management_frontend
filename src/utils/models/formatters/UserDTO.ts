import { BaseEntity } from '../base';
export interface IUserDetails extends BaseEntity {
  firstname: string;
  lastname: string;
  phoneNumber: string;
  address: string;
  joinedDate: Date;
  type: string;
}

export const UserDTO = {
  receive: (data: IUserDetails) => {
    return {
      id: data.id,
      firstname: data.firstname,
      lastname: data.lastname,
      phoneNumber: data.phoneNumber,
      address: data.address,
      joinedDate: data.joinedDate,
      type: data.type,
    };
  },
  send: (data: IUserDetails) => {
    return {
      firstname: data.firstname,
      lastname: data.lastname,
      phoneNumber: data.phoneNumber,
      address: data.address,
      joinedDate: data.joinedDate,
      type: data.type,
    };
  },
  create: (data: IUserDetails) => {
    return {
      firstname: data.firstname,
      lastname: data.lastname,
      phoneNumber: data.phoneNumber,
      address: data.address,
      joinedDate: data.joinedDate,
      type: data.type,
    };
  },
};
