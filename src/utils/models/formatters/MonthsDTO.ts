export const MonthsDTO = {
  receive: (data) => {
    return {
      id: data.id,
      months: data.months,
      expenses: data.expenses,
      createdAt: data.createdAt,
    };
  },
  send: (data) => {
    return {
      months: data.months,
      expenses: data.expenses,
    };
  },
  create: (data) => {
    return {
      months: data.months,
      expenses: data.expenses,
      createdAt: data.createdAt,
    };
  },
};
