import { errorNotify, infoNotify } from 'src/utils/notify';
import {
  DeleteRequest,
  GetRequest,
  PostRequest,
  PutRequest,
} from 'src/plugins/http';

const baseState = (
  sendName: string,
  DTO: any = {},
  subRouter: any = [],
  subRouterDTO: any = []
) => {
  const name: string = sendName
    .replace(/\//g, '_')
    .replace(/-/g, '_')
    .trim()
    .toLowerCase();
  const router: string = sendName.trim().toLowerCase();
  const subRouterRoutes = subRouter.map((d) => d.trim().toLowerCase());
  const subRouterName = subRouter.map((d) =>
    d.replace(/\//g, '_').replace(/-/g, '_').trim().toLowerCase()
  );
  const base = {
    BASEState: {
      [`${name}List`]: null,
      [`${name}`]: null,
    },
    BASEMutations: {
      [`set${name}List`](state, payload) {
        state[`${name}List`] = payload;
      },
      [`set${name}`](state, payload) {
        state[`${name}`] = payload;
      },
    },
    BASEActions: {
      [`getAll${name}`]: async ({ commit }, data) => {
        try {
          const res = await GetRequest(`/${router}`, data);
          if (!res.length) {
            infoNotify('No data to display on ' + name.replace(/_/g, ' '));
            return 0;
          }
          const send = res?.map((data) => DTO?.receive(data)) || [];
          commit(`set${name}List`, send);
        } catch (e) {
          errorNotify(`Could not get ${name.replace(/_/g, ' ')}`, e);
        }
      },
      [`getOne${name}`]: async ({ commit }, data) => {
        try {
          const res = await GetRequest(`/${router}/${data.id}`, data);
          if (!res.length) {
            infoNotify('No data to display on ' + name.replace(/_/g, ' '));
            return 0;
          }
          const send = DTO.receive(res.data);
          commit(`set${name}`, send);
        } catch (e) {
          errorNotify(`Could not get ${name.replace(/_/g, ' ')}`, e);
        }
      },
      [`getParams${name}`]: async ({ commit }, data) => {
        try {
          const res = await GetRequest(`/${router}/${data.id}`, data);
          const send = res?.map((data) => DTO?.receive(data)) || [];
          commit(`set${name}`, send);
        } catch (e) {
          errorNotify(`Could not get ${name.replace(/_/g, ' ')}`, e);
        }
      },
      [`post${name}`]: async ({ dispatch }, data) => {
        console.log(data);
        try {
          const send = DTO.create(data);
          const res = await PostRequest(`/${router}`, send);
        } catch (e) {
          errorNotify(`Could not add ${name.replace(/_/g, ' ')}`, e);
        } finally {
          dispatch(`getAll${name}`);
        }
      },
      [`put${name}`]: async ({ dispatch }, data) => {
        try {
          const { id, change } = data;
          const send = DTO.send(change);
          const res = await PutRequest(`/${router}/${id}`, send);
        } catch (e) {
          errorNotify(`Could not update ${name.replace(/_/g, ' ')}`, e);
        } finally {
          dispatch(`getAll${name}`);
        }
      },
      [`putNotId${name}`]: async ({ dispatch }, data) => {
        try {
          const send = DTO.send(data);
          const res = await PutRequest(`/${router}`, send);
        } catch (e) {
          errorNotify(`Could not update ${name.replace(/_/g, ' ')}`, e);
        } finally {
        }
      },
      [`delete${name}`]: async ({ dispatch }, id) => {
        try {
          const res = await DeleteRequest(`/${router}/${id}`);
        } catch (e) {
          errorNotify(`Could not delete ${name.replace(/_/g, ' ')}`, e);
        } finally {
          dispatch(`getAll${name}`);
        }
      },
      [`deleteSoft${name}`]: ({ dispatch }, data) => {
        try {
          const { id, change } = data;
        } catch (e) {
          errorNotify(`Could not soft delete ${name.replace(/_/g, ' ')}`, e);
        } finally {
          dispatch(`getAll${name}`);
        }
      },
    },
  };
  if (!subRouter && !subRouterDTO) {
    return base;
  }
  subRouter.forEach((val, key) => {
    base.BASEState[`${name + '_' + subRouterName[key]}List`] = null;
    base.BASEState[`${name + '_' + subRouterName[key]}`] = null;
    base.BASEMutations = {
      ...base.BASEMutations,
      [`set${name + '_' + subRouterName[key]}List`](state, payload) {
        state[`${name + '_' + subRouterName[key]}List`] = payload;
      },
      [`set${name + '_' + subRouterName[key]}`](state, payload) {
        state[`${name + '_' + subRouterName[key]}`] = payload;
      },
    };
    base.BASEActions = {
      ...base.BASEActions,
      [`getAll${name + '_' + subRouterName[key]}`]: async (
        { commit },
        data
      ) => {
        try {
          const res = await GetRequest(
            `/${name}/${subRouterRoutes[key]}`,
            data
          );
          const send =
            res?.map((data) => subRouterDTO[key]?.receive(data)) || [];
          commit(`set${name + '_' + subRouterName[key]}List`, send);
        } catch (e) {
          errorNotify(
            `Could not get ${name.replace(/_/g, ' ')} ${subRouterName[
              key
            ].replace(/_/g, ' ')}`,
            e
          );
        }
      },
      [`getOne${name + '_' + subRouterName[key]}`]: async (
        { commit },
        data
      ) => {
        try {
          const res = await GetRequest(
            `/${name}/${subRouterRoutes[key]}/${data.id}`,
            data
          );

          const send = await subRouterDTO[key].receive(res);
          commit(`set${name + '_' + subRouterName[key]}`, send);
        } catch (e) {
          errorNotify(
            `Could not get ${name.replace(/_/g, ' ')} ${name.replace(
              /_/g,
              ' '
            )}`,
            e
          );
        }
      },
      [`getProp${name + '_' + subRouterName[key]}`]: async (
        { commit },
        data
      ) => {
        try {
          const res = await GetRequest(
            `/${name}/${subRouterRoutes[key]}/${data.id}`,
            data
          );
          const send =
            (await res?.map((data) => subRouterDTO[key]?.receive(data))) || [];
          commit(`set${name + '_' + subRouterName[key]}List`, send);
        } catch (e) {
          errorNotify(
            `Could not get ${name.replace(/_/g, ' ')} ${name.replace(
              /_/g,
              ' '
            )}`,
            e
          );
        }
      },
      [`post${name + '_' + subRouterName[key]}`]: async (
        { dispatch },
        data
      ) => {
        try {
          const send = DTO.create(data);
          const res = await PostRequest(
            `/${name}/${subRouterRoutes[key]}`,
            send
          );
        } catch (e) {
          errorNotify(
            `Could not add ${name.replace(/_/g, ' ')} ${name.replace(
              /_/g,
              ' '
            )}`,
            e
          );
        } finally {
          dispatch(`getAll${name + '_' + subRouterName[key]}`);
        }
      },
    };
  });
  return base;
};
export default baseState;
