// export const multipleSearch=( inputFields= {name: '',area: '',   ph: ''},baseArray= [])=> {
//     let filteredData = baseArray;
//     for (const reqKey in inputFields) {
//       if (inputFields[reqKey]) {
//         filteredData = filteredData.filter((filterVal) => {
//           let log = filterVal[reqKey]
//             .toLowerCase()
//             .search(inputFields[reqKey].toLowerCase());
//           if (log >= 0) {
//             return true;
//           }
//         });
//       }
//     }
//    return filteredData;
//   }



export const multipleSearch=( inputFields= {name: '',area: '',   ph: ''},baseArray= [])=> {
  let filteredData = baseArray;
  for (const reqKey in inputFields) {
     !!inputFields[reqKey] &&(
      filteredData = filteredData.filter((filterVal) =>
         filterVal[reqKey]
          .toLowerCase()
          .search(inputFields[reqKey].toLowerCase()) >=0
      )
    )
  }
  return filteredData;
}
