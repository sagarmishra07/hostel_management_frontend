import { GetterTree } from 'vuex';
import { StateInterface } from 'src/store';
import baseState from 'src/utils/baseState';

import { UserDTO } from 'src/utils/models/formatters/UserDTO';
const { BASEState, BASEActions, BASEMutations } = baseState(
  'userdetails',
  UserDTO
);

export interface IState {
  exampleData: string;
}

//Store Start
const getters: GetterTree<IState, StateInterface> = {};
const state = {
  ...BASEState,
};
const mutations = {
  ...BASEMutations,
};
const actions = {
  ...BASEActions,
};

export default {
  getters,
  state,
  mutations,
  actions,
};
