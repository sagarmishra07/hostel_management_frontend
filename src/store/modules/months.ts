import { GetterTree } from 'vuex';
import { StateInterface } from 'src/store';
import baseState from 'src/utils/baseState';

import { MonthsDTO } from 'src/utils/models/formatters/MonthsDTO';
const { BASEState, BASEActions, BASEMutations } = baseState(
  'months',
  MonthsDTO
);

export interface IState {
  exampleData: string;
}

//Store Start
const getters: GetterTree<IState, StateInterface> = {};
const state = {
  ...BASEState,
};
const mutations = {
  ...BASEMutations,
};
const actions = {
  ...BASEActions,
};

export default {
  getters,
  state,
  mutations,
  actions,
};
