import { GetRequest, PostRequest } from 'src/plugins/http';

export const APIGetmonths = () => {
  return GetRequest('/localhost:8181/api/months');
};

export const APIAddmonths = (data) => {
  return PostRequest('/localhost:8181/api/months/create', data);
};
